﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testing.PageObjects
{
    public abstract class Page
    {
        public abstract string Url { get; }
        public void GoToUrl() => Browser.WebDriver.Navigate().GoToUrl(Url);
        public void Refresh() => Browser.WebDriver.Navigate().Refresh();
        protected IWebDriver driver => Browser.WebDriver;
    }
}
