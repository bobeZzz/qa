﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using System.IO;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using Faker;


namespace Testing.PageObjects
{
    public class MainPage : Page
    {
        public MainPage() { }
        public override string Url => "https://www.name-generator.org.uk/nickname/";

        public void randomizeFirstName()
        {
            driver.FindElement(By.CssSelector("input[name='first_name']")).SendKeys(NameFaker.FirstName());
        }

        public void randomizeMiddleName()
        {
            driver.FindElement(By.CssSelector("input[name='middle_name']")).SendKeys(NameFaker.FemaleFirstName());
        }

        public void randomizeLastName()
        {
            driver.FindElement(By.CssSelector("input[name='surname']")).SendKeys(NameFaker.LastName());
        }

        public void randomizeFirstAdjective()
        {
            driver.FindElement(By.CssSelector("input[name='adj1']")).SendKeys(InternetFaker.Domain());
        }

        public void randomizeSecondAdjective()
        {
            driver.FindElement(By.CssSelector("input[name='adj2']")).SendKeys(CompanyFaker.Name());
        }

        public void randomizeThirdAdjective()
        {
            driver.FindElement(By.CssSelector("input[name='adj3']")).SendKeys(StringFaker.Alpha(5));
        }

        public void submit()
        {
            driver.FindElement(By.CssSelector("input[type='submit']")).Submit();
        }
    }
}
