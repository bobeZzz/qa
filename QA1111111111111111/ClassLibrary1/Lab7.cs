﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;

namespace Testing
{
    [TestFixture]
    public class Empty
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            Browser.SetupChromeWebDriver();
            baseURL = "https://www.blazedemo.com/";
            verificationErrors = new StringBuilder();
            
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                Browser.WebDriver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }


        [Test]
        public void GenerateNickname()
        {
            var page = Pages.MainPage;
            page.GoToUrl();


            page.randomizeFirstName();
            page.randomizeMiddleName();
            page.randomizeLastName();
            page.randomizeFirstAdjective();
            page.randomizeSecondAdjective();
            page.randomizeThirdAdjective();

            ((ITakesScreenshot)Browser.WebDriver).GetScreenshot().SaveAsFile("E:\\screenshot1.png", ScreenshotImageFormat.Png);

            page.submit();

            ((ITakesScreenshot)Browser.WebDriver).GetScreenshot().SaveAsFile("E:\\screenshot2.png", ScreenshotImageFormat.Png);
        }

    }
}
