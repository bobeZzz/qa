﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SeleniumExtras.PageObjects;

namespace Testing
{
    public static class Pages
    {
        private static T getPages<T>() where T : new()
        {
            var page = new T();
            PageFactory.InitElements(page, new DefaultElementLocator(Browser.WebDriver));
            return page;
        }

        public static PageObjects.MainPage MainPage => getPages<PageObjects.MainPage>();
    }
}
