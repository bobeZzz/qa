﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class HomePage
    {

        private IWebDriver driver;

        public HomePage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "#content")]
        private IWebElement content;

        [FindsBy(How = How.CssSelector, Using = "div.news-archive")]
        private IWebElement contentArchive;

        public void goToPage()
        {
            driver.Navigate().GoToUrl("https://ztu.edu.ua/");
        }

        public ContentArchivePage goToAboutPage()
        {
            contentArchive.Click();
            return new ContentArchivePage(driver);
        }

    }
}