﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POMExample.PageObjects
{
    class ContentArchivePage
    {

        private IWebDriver driver;
        private WebDriverWait wait;

        public ContentArchivePage(IWebDriver driver)
        {
            this.driver = driver;
            driver.Manage().Window.Size = new Size(1024, 768);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "#search-2 input")]
        private IWebElement searchInput;


        public ResultPage searchText(string text)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#search-2 input"))).Click();
            searchInput.SendKeys(text);
            searchInput.Submit();
            return new ResultPage(driver);
        }

    }
}