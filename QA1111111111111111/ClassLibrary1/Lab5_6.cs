﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using POMExample.PageObjects;

namespace SeleniumTests
{
    [TestFixture]
    public class Record12241925351Am
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new ChromeDriver();
            baseURL = "https://www.blazedemo.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void SearchTextFromAboutPage()
        {
            HomePage home = new HomePage(driver);
            home.goToPage();
            ContentArchivePage about = home.goToAboutPage();
            ResultPage result = about.searchText("випускники");
            result.clickOnFirstArticle();
        }

        [Test]
        public void IncorrectCredentials()
        {
            // Label: Test
            // ERROR: Caught exception [ERROR: Unsupported command [resizeWindow | 1920,920 | ]]
            driver.Navigate().GoToUrl("https://learn.ztu.edu.ua/");
            driver.FindElement(By.LinkText("Вхід")).Click();
            driver.FindElement(By.Id("username")).Click();
            driver.FindElement(By.Id("username")).Clear();
            driver.FindElement(By.Id("username")).SendKeys("sadasdasd");
            driver.FindElement(By.Id("password")).Click();
            driver.FindElement(By.Id("password")).Clear();
            driver.FindElement(By.Id("password")).SendKeys("sghfhjufgjfhg");
            driver.FindElement(By.Id("loginbtn")).Click();
            var delay = 2;
            var now = DateTime.Now;

            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(delay));
            wait.Until(wd => (DateTime.Now - now) - TimeSpan.FromSeconds(delay) > TimeSpan.Zero);
            Assert.AreEqual(driver.FindElement(By.CssSelector("div.alert.alert-danger")).Text, "Ви не пройшли ідентифікацію (неправильне ім’я користувача або пароль).<br />Спробуйте ще раз.");
        }

        [Test]
        public void IsRussianLocaleWorking()
        {
            // Label: Test
            // ERROR: Caught exception [ERROR: Unsupported command [resizeWindow | 1920,920 | ]]
            driver.Navigate().GoToUrl("https://learn.ztu.edu.ua/?lang=ru");
            var isRu = driver.FindElement(By.CssSelector(".dropdown-toggle.nav-link")).Text.Contains("ru");
            Assert.IsTrue(isRu);
            Assert.AreEqual(driver.FindElement(By.Id("frontpage-category-combo")).FindElement(By.TagName("h2")).Text, "Курсы");
        }

        [Test]
        public void IsEnglishLocaleWorking()
        {
            // Label: Test
            // ERROR: Caught exception [ERROR: Unsupported command [resizeWindow | 1920,920 | ]]
            driver.Navigate().GoToUrl("https://learn.ztu.edu.ua/?lang=en");
            var isEn = driver.FindElement(By.CssSelector(".dropdown-toggle.nav-link")).Text.Contains("en");
            Assert.IsTrue(isEn);
            Assert.AreEqual(driver.FindElement(By.Id("frontpage-category-combo")).FindElement(By.TagName("h2")).Text, "Courses");
        }

        [Test]
        public void NoOneIsOnline()
        {
            // Label: Test
            // ERROR: Caught exception [ERROR: Unsupported command [resizeWindow | 1920,920 | ]]
            driver.Navigate().GoToUrl("https://learn.ztu.edu.ua/course/view.php?id=42");
           
            Assert.AreEqual(driver.FindElement(By.CssSelector(".card-text.content.mt-3 div")).Text, "No online users (останні 5 хвилин)");
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
